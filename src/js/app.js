import Card from "./Components/Card.js";
import createElement from "./functions/create_element.js";

const app = document.getElementById("app");
const buttonNewPost = document.querySelector(".new-post");

buttonNewPost.addEventListener("click", () => {
  const modalWindow = createElement("div", "modal-window");
  const titleInput = createElement("input", "title-input");
  const bodyInput = createElement("textarea", "body-input");
  const createNewPost = createElement("button", "create-post-button");
  createNewPost.innerText = "Create post";
  const cancelCreateNewPost = createElement("button", "cancel-post-button");
  cancelCreateNewPost.innerText = "Cancel";

  modalWindow.append(titleInput, bodyInput, createNewPost, cancelCreateNewPost);
  app.append(modalWindow);

  cancelCreateNewPost.addEventListener("click", () => {
    modalWindow.remove();
  });

  createNewPost.addEventListener("click", (event) => {
    createNewPost.disabled = true;
    cancelCreateNewPost.disabled = true;
    const titleInputValue = titleInput.value;
    const bodyInputValue = bodyInput.value;
    fetch("https://ajax.test-danit.com/api/json/posts", {
      method: "GET",
    })
      .then((response) => response.json())
      .then((postsArray) => {
        const lastPost = postsArray.reduce((prev, current) => {
          return prev.id > current.id ? prev.id : current.id;
        });

        const currentPostId = lastPost + 1;

        fetch("https://ajax.test-danit.com/api/json/posts", {
          method: "POST",
          body: JSON.stringify({
            userId: 1,
            id: currentPostId,
            title: titleInputValue,
            body: bodyInputValue,
          }),
          headers: {
            "Content-Type": "application/json",
          },
        }).then((response) => {
          if (response.ok) {
            const newPost = new Card({
              body: bodyInputValue,
              title: titleInputValue,
              id: currentPostId,
              userId: 1,
            });
            newPost.createPost(app, "afterbegin");
            modalWindow.remove();
          }
        });
      });
  });
});

const loadingPage = createElement("div", "loading-page");

app.append(loadingPage);

fetch("https://ajax.test-danit.com/api/json/posts", {
  method: "GET",
})
  .then((response) => {
    response.ok && loadingPage.remove();
    return response.json();
  })
  .then((postsArray) => {
    postsArray.map((post) => {
      const newPost = new Card(post);
      newPost.createPost(app, "beforeend");
    });
  });
